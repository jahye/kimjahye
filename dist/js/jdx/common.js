window.Jdx = {};
(function($){
    var func = {};
    func.ojj = {
    	init: function(){
            var _self = this;
            _self.categoryFunc();
            _self.goTop();
            _self.popupView();
            // _self.categoryBtnPos();//190628 삭제
            _self.mainPopClose();
            _self.submenuView();
            _self.myFavorate();
            _self.searchView();
            _self.loginTabToggle();
    	},
        categoryFunc: function(){
            var $wrap = $('.jdxWrapper'),
                $btn_wr = $('.h_fixedMenu'),
                $btn = $btn_wr.find('.func_viewmenu'),
                $category = $('.h_fixedMenuLayer'),
                $btn_close = $category.find('.func_close'),
                $depth1_li = $category.find('.dth1_menu_tab li'),
                $depth1 = $depth1_li.find('a'),
                $depth2_category = $category.find('.dth2_list'),
                $depth2 = $category.find('.dth2_menu'),
                $depth3_category = $category.find('.dth3_list'),
                $class = 'is-active',
                $onPregress = false;
            $btn.on('click', function(e){//카테고리 열기
                e.preventDefault();
                $onPregress = true;
                $category.addClass($class);
                $wrap.delay(500).queue(function(){
                    $wrap.addClass('is-hidden').dequeue();
                    $onPregress = false;
                });
            });
            $btn_close.on('click', function(e){//카테고리 닫기
                e.preventDefault();
                if ($onPregress) {
                    return false;
                }
                $category.removeClass($class);
                $wrap.removeClass('is-hidden');

                $depth3_category.stop(false,true).slideUp('fast');
            });
            $depth1.on('click', function(e){//카테고리 선택
                e.preventDefault();
                var $this = $(this), 
                    $idx = $this.parent().index();
                $depth1.removeClass('on');
                $this.addClass('on');
                $depth2_category.removeClass($class);
                $depth2_category.eq($idx).addClass($class);
            });
            $depth2.on('click', function(e){//뎁스2 카테고리 선택
                e.preventDefault();
                var $this = $(this),
                    $next = $this.next();
                if ($this.parent().hasClass($class)) {
                    $next.stop(false,true).slideUp('fast');
                    $this.parent().removeClass($class);
                } else {
                    $depth3_category.stop(false,true).slideUp('fast');
                    $next.stop(false,true).slideDown('fast');
                    $depth2_category.find('.dth2_li').removeClass($class);
                    $this.parent().addClass($class);
                }
            });
        },
    	goTop: function(){
    		var $btn = $('.h_gotoTop');
    		$btn.on('click', function(e){
    			e.preventDefault();
    			$('html, body').stop().animate({ scrollTop: 0 }, 400);
    		});
    	},
        popupView: function(){//190719 팝업 수정
            var $wrap = $('.jdxWrapper'),
                $onPregress = false,
                $btn_viewer = $('.func-popup-viewer'),
                $btn_close = $('.jdxPopupWrapper .func-cls');

            $btn_close.on('click', function(e){
                e.preventDefault();
                if ($onPregress) {
                    return false;
                }
                $(this).closest('.jdxPopupWrapper').removeClass('is-active');
                console.log($('.jdxPopupWrapper.is-active').length);
                if ($('.jdxPopupWrapper.is-active').length == 0) {//190719 추가
                    $wrap.removeClass('is-hidden');
                }
            });
            $btn_viewer.on('click', function(e){
                e.preventDefault();
                $onPregress = true;
                var $class = $('.' + $(this).attr('data-popup'));
                $class.addClass('is-active');
                $wrap.delay(500).queue(function(){
                    $wrap.addClass('is-hidden').dequeue();
                    $onPregress = false;
                });
            });
            
        },
        // categoryBtnPos: function(){//190628 탑버튼, 플로팅 메뉴 hide & view 삭제
        //     var $win = $(window), 
        //         $st = $win.scrollTop(),
        //         $category = $('.h_fixedMenu'),
        //         $footer = $('footer'),
        //         $footer_ot = $footer.offset().top - $win.height(),
        //         $class = 'is-hidden',
        //         $onPregress = false;

        //     var $btn = $('.h_gotoTop');
        //     $btn.on('click', function(e){
        //         e.preventDefault();
        //         // $onPregress = true;
        //         $(window).scrollTop(0);
        //         // $('html, body').stop().animate({ scrollTop: 0 }, 0, function(){
        //         //     // $onPregress = false;
        //         // });
        //     });
            
        //     $win.on('scroll', function(){
        //         var $this = $(this),
        //             $new_st = $this.scrollTop();
        //         // if (!$onPregress) {
        //             if ($new_st > $st) {
        //                 // console.log('scrollDown');
        //                 $category.addClass($class);
        //                 if ($new_st >= $footer_ot) {
        //                     $category.removeClass($class);
        //                 }
        //             } else {
        //                 // console.log('scrollUp');
        //                 $category.removeClass($class);
        //             }  
        //         // }
        //         if ($new_st <= 0) {
        //             $category.removeClass($class);
        //         }
        //         $st = $new_st;
        //     });
        // },
        mainPopClose: function (){//190621 메인페이지 팝업 닫기
            var $btn = $('.h_winPop .bt-tp-cls');
            $btn.on('click', function(e){
                e.preventDefault();
                $(this).closest('.h_winPop').hide();
            });
        },
        submenuView: function(){//190621 타이틀 클릭하면 서브메뉴 노출
            var $btn_wr = $('.comm-titlearae');
            if ($btn_wr.length <= 0) {
                return false;
            }
            $btn_wr.addClass('is-hidden');
            var $btn = $btn_wr.find('.cm-title a.tgl'),
                $onPregress = false;
            $btn.on('click', function(e){
                e.preventDefault();
                if ($onPregress) {
                    return false;
                }
                $onPregress = true;
                var $this = $(this);
                if ($this.hasClass('is-active')) {
                    $btn_wr.delay(150).queue(function(){
                        $btn_wr.addClass('is-hidden').dequeue();
                        $onPregress = false;
                    });
                    $this.removeClass('is-active');
                    $this.closest('.comm-titlearae').removeClass('is-active');
                    
                } else {
                    $onPregress = true;
                    $this.addClass('is-active');
                    $this.closest('.comm-titlearae').addClass('is-active');
                    $btn_wr.removeClass('is-hidden');
                    $btn_wr.delay(150).queue(function(){
                        $btn_wr.dequeue();
                        $onPregress = false;
                    });
                }
            });
        },
        myFavorate: function(){//190621 관심상품 버튼 토글링
            // bt-tp-favor
            var $btn = $('.bt-tp-favor');
            $btn.on('click', function(e){
                e.preventDefault();
                var $this = $(this),
                    $class = 'is-active';

                if ($this.hasClass($class)) {
                    $this.removeClass($class);
                } else {
                    $this.addClass($class);
                }

            });
        },
        searchView: function(){//190719 검색
            var $search = $('.h_search a'),
                $search_l = $('.h_searchLayer'),
                $close = $search_l.find('.func_close'),
                $a_class = 'is-active',
                $s_class = 'is-show',
                $onPregress = false;
            $search.on('click', function(e){
                e.preventDefault();
                $search_l.addClass($s_class);
                $search_l.addClass($a_class);
            });
            $close.on('click', function(e){
                e.preventDefault();
                $search_l.removeClass($a_class);
                $search_l.delay(150).queue(function(){
                    $search_l.removeClass($s_class).dequeue();
                });
            });
        },
        loginTabToggle: function(){//190719 로그인 탭 토글링
            var $popup = $('.popup-login-input'),
                $tab = $popup.find('.reg-tab a'),
                $layer = $popup.find('.reg-tabconnnnt .conteents');
            $tab.on('click', function(e){
              e.preventDefault();
              var $this = $(this),
                    $num = $this.parent().index(),
                    $class = 'is-active';
              $tab.removeClass('on');
              $this.addClass('on');
              $layer.removeClass($class);
              $layer.eq($num).addClass($class);
            });
        }
    };
    Jdx = func.ojj;
    Jdx.init();
})(jQuery);

function login_popup_tab_check ($num){//190719 로그인, 비회원 주문조회 링크 체크
    var $popup = $('.popup-login-input'),
        $tab = $popup.find('.reg-tab'),
        $layer = $popup.find('.reg-tabconnnnt .conteents');
    if ($num == null || $num == '') {
        $num = 0;
    }
    $tab.find('a').removeClass('on');
    $tab.find('li').eq($num).find('a').addClass('on');
    $layer.removeClass('is-active');
    $layer.eq($num).addClass('is-active');
}

$(document).ready(function () {
    //모바일 네비
    mobileNav();

});



//모바일 메뉴
function mobileNav() {
    var $tar = $("h2.content_title");
    var $tarList = $(".allMenu");

    $tar.on("click", function () {
        $tarList.slideToggle("fast");
        $(".content_title").toggleClass("-active");
    });

}