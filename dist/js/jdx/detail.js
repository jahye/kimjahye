window.Jdx_Detail = {};
(function($){
    var func = {};
    func.ojj = {
    	init: function(){
            var _self = this;
    		_self.detailJquery();
    	},
        detailJquery: function(){
          var $tab = $('.cm-tab li a'),
              $tooltip = $('.bt-tp-tooltip'),
              $selection = $('.t-select .ts-bt'),
              $direct_quick = $('.product_quickMenus'),
              $info = $('.product_detail .detail_info'),
              $class = 'is-active',
              $buy_btn = $('.product_quickMenus .p-foot .b-buy, .product_quickMenus .p-foot .b-cart');

            //상세 슬라이드
            var main_swiper = new Swiper('.product_detail_slider.swiper-container', {
              pagination: {
                el: '.swiper-pagination',
                type: 'progressbar',
              },
              speed: 400,
            });//상세 슬라이드 end

            var recom_swiper = new Swiper('.product_recomSlide .swiper-container', {//추천상품 슬라이드
                slidesPerView: 'auto',
                spaceBetween: 8,
                centeredSlides: true,
                loop: true,
                speed: 400,
                observer: true,
                observeParents: true,
            });

            $('.product_tl li a').on('click', function(e){//추천상품 슬라이드 토글링
              e.preventDefault();
              var $this = $(this), 
                  $idx = $this.parent().index(),
                  $slide = $('.product_recomSlide .thumbnail-list');
              $('.product_tl li a').removeClass('on');
              $this.addClass('on');
              $slide.removeClass($class);
              $slide.eq($idx).addClass($class);
            });

            var special_special = new Swiper('.recomspecial-view', {//SPECIAL 슬라이드
              slidesPerView: 'auto',
              spaceBetween: 25,
              speed: 400,
            });

            $('.product_detail_info .box_wrap a.tab').on('click', function(e){//구매정보 토글링
                e.preventDefault();
                var $this = $(this),
                    $off = 'off';
                // if ($this.hasClass($off)) {
                //     $('html, body').stop().animate({ scrollTop: $this.offset().top }, 400);
                // }
                $this.toggleClass($off);
                $this.next().stop(false, true).slideToggle();
            });

            $('.product_quickMenus .p-foot .b-share').on('click', function(e){//공유 버튼 토글링
              e.preventDefault();
              var $popup = $('.popup-sns-view');
              $popup.addClass($class);
            });

            $('.product_tab_detail .product_tab').on('click', function(e){//상세정보 탭 토글링
              e.preventDefault();
              var $this = $(this);
              if ($this.attr('data-popup') == undefined) {

                if ($this.hasClass('on')) {
                    $this.removeClass('on');
                    $this.next().removeClass($class);
                } else {
                    $this.addClass('on');
                    $this.next().addClass($class);
                    $('html, body').stop().animate({ scrollTop: $this.offset().top }, 400);  
                }  
              }
            });

            $buy_btn.on('click', function(e){//퀵버튼(장바구니/바로구매) 옵션영역 활성화
                e.preventDefault();
                $(this).parent().addClass($class);
                $(this).closest('.product_quickMenus').addClass($class);
            });
            $('.product_quickMenus .p-conteeents .func-cls').on('click', function(e){//퀵버튼(장바구니/바로구매) 옵션영역 비활성화
                e.preventDefault();
                $(this).closest('.product_quickMenus').removeClass($class);
                $buy_btn.parent().removeClass($class);
            });

            $('.t-selecteeeeeed').on('click', function(e){//상품 선택
                $('.t-selecteeeeeed').removeClass($class);
                $(this).addClass($class);
            });

            var review_swiper = new Swiper('.review-list.swiper-container', {//리뷰 슬라이드
                slidesPerView: 'auto',
                spaceBetween: 0,
                speed: 400,
            });

            $('.qna-tb-list .qa_sub').on('click', function(){//문의 리스트 토글링
              $(this).parent().toggleClass($class);
            });

            $('.input_etc.email select').on('change', function(){//이메일 도메인 직접입력 선택
              var $this = $(this);
              if ($this.val() == '직접입력') {
                $this.closest('.input_etc.email').find('.direct').val('');
                $this.closest('.input_etc.email').find('.direct').attr('readonly', false);
              } else {
                $this.closest('.input_etc.email').find('.direct').val($this.val());
                $this.closest('.input_etc.email').find('.direct').attr('readonly', true);
              }
            });

            $('.popup-search-shop .lst_searched .map').on('click', function(e){//상품 보유매장 지도 토글링
              e.preventDefault();
              $('.popup-search-shop .lst_searched .map').addClass('light');
              $(this).removeClass('light');
              $('.popup-search-shop .lst_searched .hide_bubun').removeClass($class);
              $(this).closest('li').find('.hide_bubun').addClass($class);
            });

        }
    };
    Jdx_Detail = func.ojj;
    Jdx_Detail.init();
})(jQuery);

