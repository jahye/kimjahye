$(function () {
    $(".tabContent").hide();
    $(".tabContent:first").show();
    $(".tabNav a").click(function () {
        $(this).addClass("-active").siblings().removeClass("-active");
        $(".tabContent").hide()
        var activeTab = $(this).attr("rel");
        $("#" + activeTab).fadeIn()
    });

});

