$( function() {
    listFilterFunction();
    function listFilterFunction(){
        var $area = $( "#slider-range" ),
            $min_val = 0,//최저 가격대
            $max_val = 600000,//최고 가격대
            $btn = $('.filter_viewer'),
            $finter = $('.popup-filter-view'),
            $close = $finter.find('.func-cls'),
            $reset = $finter.find('.func-reset'),
            $c_all = $('.color-comm-chip .c-chip.all input'),
            $c_chip = $('.color-comm-chip .c-chip'),
            $c_input = $c_chip.find('input'),
            $s_all = $('.size-comm-chip .s-chip.all input'),
            $s_chip = $('.size-comm-chip .s-chip'),
            $s_input = $s_chip.find('input');

        if ($finter.length == 0) {
            return false;
        }
        //필터 가격대 선택
        $area.slider({
          range: true,
          min: $min_val,//최저 가격대
          max: $max_val,//최고 가격대
          values: [ $min_val, $max_val ],//최저, 최고 가격대
          slide: function( event, ui ) {
            $("#slider-range .ui-slider-handle").eq(0).find('b').text(priceFormat(ui.values[0]));
            $("#slider-range .ui-slider-handle").eq(1).find('b').text(priceFormat(ui.values[1]));
            if(priceCollision($("#slider-range .ui-slider-handle").eq(0), $("#slider-range .ui-slider-handle").eq(1))) {
              $("#slider-range .ui-slider-handle").eq(1).find('span').css({marginTop:30})
            } else {
              $("#slider-range .ui-slider-handle").eq(1).find('span').css({marginTop:10});
            }
          }
        });
        $("#slider-range .ui-slider-handle").append('<span><b></b>원</span>');
        $("#slider-range .ui-slider-handle").eq(0).find('b').text(priceFormat($area.slider( "values", 0 )));
        $("#slider-range .ui-slider-handle").eq(1).find('b').text(priceFormat($area.slider( "values", 1 )));
        //필터 가격대 선택 end

        // $btn.on('click', function(e){//필터 열기
        //   e.preventDefault();
        //   $finter.addClass('is-active');
        //   $('.filter_op').addClass('is-active');
        // });

        // //필터 닫기
        // $close.on('click', function(e){
        //   e.preventDefault();
        //   $finter.removeClass('is-active');
        //   $('.filter_op').removeClass('is-active');
        // });
        // $('.filter_op').on('click', function(e){
        //   e.preventDefault();
        //   $finter.removeClass('is-active');
        //   $('.filter_op').removeClass('is-active');
        // });//필터 닫기 end

        $reset.on('click', function(e){//초기화 버튼
          e.preventDefault();

          $c_input.prop('checked', false);
          $c_all.prop('checked', true);
          $s_input.prop('checked', false);
          $s_all.prop('checked', true);

          $finter.find('.season-chip input').prop('checked', false);//모든 시즌 체크 해제
          $finter.find('.season-chip .input_button').eq(1).find('input').prop('checked', true);//현재 시즌 체크

          $finter.find('.line-chip input').prop('checked', false);

          $("#slider-range .ui-slider-handle").eq(1).find('span').css({marginTop:10});
          $area.slider({values: [ $min_val, $max_val ]});
          $("#slider-range .ui-slider-handle").eq(0).find('b').text(priceFormat($area.slider( "values", 0 )));
          $("#slider-range .ui-slider-handle").eq(1).find('b').text(priceFormat($area.slider( "values", 1 )));
        });
        
        //컬러 all 버튼
        $c_input.on('change', function(){
          var $this = $(this);
          if ($this.parent().attr('class').indexOf('all') == -1) {
            $c_all.prop('checked', false);
          }
        });
        $c_all.on('change', function(){
          var $this = $(this);
          if ($this.prop('checked')) {
            $c_chip.each(function(){
              if ($(this).attr('class').indexOf('all') == -1) {
                $(this).find('input').prop('checked', false);  
              }
            });
          }
        });//컬러 all 버튼 end
        //사이즈 all 버튼
        $s_input.on('change', function(){
          var $this = $(this);
          if ($this.parent().attr('class').indexOf('all') == -1) {
            $s_all.prop('checked', false);
          }
        });
        $s_all.on('change', function(){
          var $this = $(this);
          if ($this.prop('checked')) {
            $s_chip.each(function(){
              if ($(this).attr('class').indexOf('all') == -1) {
                $(this).find('input').prop('checked', false);  
              }
            });
          }
        });//사이즈 all 버튼 end
    }

    function priceFormat(str) {//가격 포맷
        str = String(str);
        return str.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
    }

    function priceCollision($div1, $div2) {//필터 가격대 선택 bar 겹치지 않게 처리
        var x1 = $div1.offset().left;
        var w1 = 70;
        var r1 = x1 + w1;
        var x2 = $div2.offset().left;
        var w2 = 70;
        var r2 = x2 + w2;

        if (r1 < x2 || x1 > r2) return false;
        return true;
    }
} );